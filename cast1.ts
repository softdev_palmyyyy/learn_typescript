let x1: unknown = 'hello';
let num: string = "1";
console.log((x1 as string).length);
console.log((<string>x1).length);
console.log(((num as unknown) as number).toFixed(2));
